<?php
/**
 * Created by PhpStorm.
 * User: Mogbeyi David
 * Date: 05/04/2017
 * Time: 11:02
 */
define('ROOT',dirname(__DIR__).DIRECTORY_SEPARATOR);
if(file_exists(ROOT.'vendor/autoload.php')){
    require ROOT.'vendor/autoload.php';
}